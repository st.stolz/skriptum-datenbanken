use w3c;

/* INSERT INTO products (products.ProductID, products.ProductName) 
VALUES (78, "Mein Superprodukt"); */
/* Würde einen FOREING KEY Constraint error auslösen: */
/* INSERT INTO products (products.ProductID, products.ProductName, products.CategoryID) 
VALUES (79, "Mein Hyperprodukt",7294727); */

SELECT
  products.ProductID,
  products.ProductName,
  products.Price,
  categories.CategoryName,
  suppliers.SupplierName
FROM
  products
  LEFT JOIN categories ON products.CategoryID = categories.CategoryID
  LEFT JOIN suppliers ON products.SupplierID = suppliers.SupplierID
ORDER BY
  products.ProductID DESC
LIMIT
  3;