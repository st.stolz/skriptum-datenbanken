SELECT  orders.OrderID as Bestellnummer, 
        orders.OrderDate, 
        orderdetails.ProductID, 
        products.ProductName, 
        orderdetails.Quantity, products.Price  
FROM orders 
JOIN orderdetails ON orders.OrderID = orderdetails.OrderID 
JOIN products ON products.ProductID = orderdetails.ProductID  
WHERE orders.OrderID = 10248;