SELECT Personen.name as Vorname, Emails.url as "E-Mail Adresse"
FROM Personen
LEFT JOIN Emails
ON Personen.id = Emails.personen_id;