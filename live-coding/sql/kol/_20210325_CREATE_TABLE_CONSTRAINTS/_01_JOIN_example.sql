-- Beispiel nach Tutorial https://www.mysqltutorial.org/mysql-join/

-- Würde die Tabellen leeren 
-- TRUNCATE members;
-- TRUNCATE committees;

-- Löscht die Tabellen
DROP TABLE IF EXISTS members;
DROP TABLE IF EXISTS committees;

CREATE TABLE members (
    member_id INT AUTO_INCREMENT,
    name VARCHAR(100),
    member_status INT,
    PRIMARY KEY (member_id)
);

CREATE TABLE committees (
    committee_id INT AUTO_INCREMENT,
    name VARCHAR(100),
    commitee_status INT,
    PRIMARY KEY (committee_id)
);

INSERT INTO members(name, member_status)
VALUES('John', 1),('Jane', 3),('Mary', 3),('David', 4),('Amelia', 2);

INSERT INTO committees(name, commitee_status)
VALUES('John', 22),('Mary', 10),('Amelia', 5),('Joe', 100);

-- SELECT 
--     m.member_id, 
--     m.name as member, 
--     m.member_status,
--     c.committee_id, 
--     c.name committee,
--     c.commitee_status
-- FROM
--     members m
-- INNER JOIN committees c 
-- 	ON c.name = m.name;

-- SELECT 
--     m.member_id, 
--     m.name as member, 
--     m.member_status,
--     c.committee_id, 
--     c.name committee,
--     c.commitee_status
-- FROM
--     members m
-- LEFT JOIN committees c 
-- 	ON c.name = m.name;

SELECT 
    m.member_id, 
    m.name as member, 
    m.member_status,
    c.committee_id, 
    c.name committee,
    c.commitee_status
FROM
    members m
LEFT JOIN committees c 
	ON c.name = m.name
WHERE c.committee_id is null;