use kol_dbi;

CREATE TABLE IF NOT EXISTS Persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255),
    State varchar(255)
);

ALTER TABLE Persons
ADD Province varchar(255);

ALTER TABLE Persons
DROP COLUMN Province;

DROP TABLE IF EXISTS Primary_Key_1;
CREATE TABLE Primary_Key_1 (
    id int not null auto_increment primary key,
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255),
    State varchar(255)
);

DROP TABLE IF EXISTS Primary_Key_2;
CREATE TABLE Primary_Key_2 (
    id int not null auto_increment,
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255),
    State varchar(255),
    primary key(id)
);



