-- Es sollen alle Bestellungen ausgegeben werden, die der Kunde mit der ID 4 getätigt hat. 
-- Hat er keine Bestellungen getätigt, muss auch nichts ausgegeben werden.
-- Falls den Bestellungen auch ein Lieferant zugeordnet wurde, soll dieser auch ausgegeben werden.


use w3c;

SELECT customers.CustomerName as Kundenname, orders.OrderID as Bestellnummer, shippers.ShipperName as Lieferant
    FROM orders 
    INNER JOIN customers ON customers.CustomerID = orders.CustomerID
    LEFT JOIN shippers ON shippers.ShipperID = orders.ShipperID
    WHERE customers.CustomerID = 4;