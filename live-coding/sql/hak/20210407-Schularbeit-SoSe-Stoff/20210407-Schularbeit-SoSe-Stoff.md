# Schularbeit 2020 / 2021 Sommersemester

## Vorbereitung

* W3C Datenbank importieren und testen
* [mysqltutorial Sample Database](https://www.mysqltutorial.org/how-to-load-sample-database-into-mysql-database-server.aspx) importieren, umbenennen in "mysqltutorial" und testen

## Logisches Design 

* ER Diagramm lesen (ERDish) und schreiben
* Entität, Instanz, Attribute, Subtypen und Supertypen
* UID, UID Kandidaten, Artificielle UIDs, Zusammengesetzte UIDs
* Beziehungen mit ihren Kardinalitäten (1:1, 1:n, n:m)
* Physikalisches Design
* Auflösung n:m Beziehung aus logischem Design für relationale Datenbank (Intersection Tabelle, oft barred)

## SQL

* Data types, Primary Keys, Foreign Keys
* CREATE TABLE mit Constraints (Primary Keys, Foreign Keys, INDEX)
* INSERT, ALTER, DROP, UPDATE Statement
* **SELECT mit verschiedene JOINS, WHERE (LIKE, Wildcards, IN, BETWEEN), GROUP BY, HAVING**
* **Funktionen mit Unterscheidung single row und multi row** (Aggregat wie COUNT, SUM, AVG, MIN, MAX, ...) - bei GROUP BY gelten Aggregatsfunktionen pro Gruppe
* **Subqueries in FROM, IN**

## Schwerpunkt Schularbeit

* Umsetzungsmöglichkeiten von Vererbung (Subentitäten im logischen Design) in einer relationalen Datenbank - [siehe JPA Beispiele](https://wiki.eclipse.org/EclipseLink/Examples/JPA/Inheritance)
* JOINs, [Subqueries](https://www.mysqltutorial.org/mysql-subquery/), Group BY, Having, Aggregate Functions, Date Functions und Math Functions
* Indexes, Views
* Variables
* [REGEXP](https://www.mysqltutorial.org/mysql-regular-expression-regexp.aspx)
* Foreign Key Constraints
