USE 18ita_testdb;

-- Achtung auf Reihenfolge!
DROP TABLE IF EXISTS Emails;
DROP TABLE IF EXISTS Persons;

CREATE TABLE Persons (
    id int not null auto_increment,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255),
    State varchar(255),
    primary key(id)
);

-- Achtung auf Reihenfolge!
CREATE TABLE Emails (
    id int not null auto_increment,
    address varchar(255),
    person_id int,
    primary key(id),
    foreign key(person_id) references Persons(id)
);

INSERT INTO Persons (LastName) VALUES ('Maar'), ('Stolz'), ('Landerer');

/* Folgender Insert funktioniert nur, wenn man checks ausschaltet: */
/* SET foreign_key_checks = 0;
INSERT INTO Emails (address, person_id) VALUES
     ('st.stolz@gmail.com', 2),
     ('s.stolz@tsn.at',2),
     ('phmaar@tsn.at', 10); */

INSERT INTO Emails (address, person_id) VALUES
    ('st.stolz@gmail.com', 2),
    ('s.stolz@tsn.at',2),
    ('landererETtsn.at',3);

SELECT * FROM Persons LEFT JOIN Emails ON Persons.id = Emails.person_id;

SELECT * FROM Persons LEFT JOIN Emails ON Persons.id = Emails.person_id WHERE Emails.address REGEXP '.*@.*';

SELECT * FROM Persons LEFT JOIN Emails ON Persons.id = Emails.person_id WHERE Emails.address NOT REGEXP '.*@.*';