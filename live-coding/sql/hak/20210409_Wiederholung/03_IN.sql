use w3c;

SELECT customers.CustomerName FROM customers 
    WHERE customers.CustomerName LIKE "Alfreds Futterkiste" 
        OR customers.CustomerName LIKE "Around the Horn";

SELECT customers.CustomerName FROM customers 
    WHERE customers.CustomerName IN ("Alfreds Futterkiste", "Around the Horn");

SELECT customers.CustomerName FROM customers 
    WHERE customers.CustomerName IN (SELECT customers.CustomerName FROM customers WHERE customers.CustomerName LIKE "A%");


SELECT customers.CustomerID, customers.CustomerName FROM customers 
    WHERE customers.CustomerID IN (SELECT orders.CustomerID FROM orders WHERE orders.OrderDate = "1996-07-08");

/* Sollte ineffizienter sein, bringt aber das selbe Ergebnis: */

SELECT customers.CustomerID, customers.CustomerName FROM customers 
    INNER JOIN orders ON orders.CustomerID = customers.CustomerID
    WHERE orders.OrderDate = "1996-07-08";